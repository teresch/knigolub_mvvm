package io.a0x11.knigolub.data.mappers

import io.a0x11.knigolub.data.model.local.Cache
import io.a0x11.knigolub.data.model.local.Favourite
import io.a0x11.knigolub.domain.model.Book
import io.a0x11.knigolub.presentation.model.ObservableItemBook
import java.util.*

fun mapBookToCache(book: Book) = Cache(
        UUID.randomUUID().toString(),
        book.bookTitle,
        book.bookUrl,
        book.author,
        book.authorUrl,
        book.bookUpdateDate,
        book.bookFullSize,
        book.bookUpdateSize,
        book.bookGenres,
        book.bookDescription,
        book.site,
        book.favourite
)

fun mapObservableItemBookToFavourite(book: ObservableItemBook) = Favourite(
        book.bookTitle,
        book.bookUrl,
        book.author,
        book.authorUrl,
        book.bookUpdateDate,
        book.bookFullSize,
        book.bookUpdateSize,
        book.bookGenres,
        book.bookDescription,
        "samlib"
)