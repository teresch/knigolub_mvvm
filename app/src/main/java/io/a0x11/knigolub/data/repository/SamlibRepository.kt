package io.a0x11.knigolub.data.repository

import io.a0x11.knigolub.data.local.room.CacheDao
import io.a0x11.knigolub.data.local.room.FavouriteDao
import io.a0x11.knigolub.data.mappers.mapBookToCache
import io.a0x11.knigolub.data.remote.Api
import io.a0x11.knigolub.domain.model.Book
import io.a0x11.knigolub.presentation.model.ObservableItemBook
import javax.inject.Inject

class SamlibRepository @Inject constructor(private val api: Api, private val favouriteDao: FavouriteDao, private val cacheDao: CacheDao) {

    fun samlibApiList() = api.samlibList()

    fun cacheBook(book: Book) = cacheDao.cacheBook(mapBookToCache(book))

    fun getCache() = cacheDao.getCache()

    fun deleteBookFromFavourite(bookUrl: String) = favouriteDao.deleteBookFromFavourite(bookUrl)

    fun getAllFavouriteBooks() = favouriteDao.getAllFavouriteBooks()

    fun clearCache() = cacheDao.clearCache()

    fun makeBookFavourite(book: ObservableItemBook) = favouriteDao.makeBookFavourite(book)

}
