package io.a0x11.knigolub.data.local.room

import android.arch.persistence.room.*
import io.a0x11.knigolub.data.mappers.mapObservableItemBookToFavourite
import io.a0x11.knigolub.data.model.local.Favourite
import io.a0x11.knigolub.presentation.model.ObservableItemBook
import io.reactivex.Single


@Dao
abstract class FavouriteDao {

    @Query("SELECT * FROM favourite ORDER BY update_date DESC")
    abstract fun getAllFavouriteBooks(): Single<List<Favourite>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun makeBookFavourite(favourite: Favourite)

    @Query("UPDATE cache SET favourite = 1 WHERE url=:url")
    abstract fun markCacheAsFavourite(url: String)

    @Query("DELETE FROM favourite WHERE url = :url")
    abstract fun deleteFavouriteBook(url: String)

    @Query("UPDATE cache SET favourite = 0 WHERE url=:url")
    abstract fun markCacheAsNonFavourite(url: String)

    @Transaction
    open fun makeBookFavourite(book: ObservableItemBook) {
        makeBookFavourite(mapObservableItemBookToFavourite(book))
        markCacheAsFavourite(book.bookUrl)
    }

    @Transaction
    open fun deleteBookFromFavourite(bookUrl: String) {
        deleteFavouriteBook(bookUrl)
        markCacheAsNonFavourite(bookUrl)
    }

}