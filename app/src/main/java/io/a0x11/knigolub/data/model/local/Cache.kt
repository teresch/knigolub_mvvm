package io.a0x11.knigolub.data.model.local

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "cache")
data class Cache (

        @PrimaryKey()
        val id: String,
        @ColumnInfo(name = "title")
        val bookTitle: String,
        @ColumnInfo(name = "url")
        val bookUrl: String,
        val author: String,
        @ColumnInfo(name = "author_url")
        val authorUrl: String,
        @ColumnInfo(name = "update_date")
        val bookUpdateDate: Long,
        @ColumnInfo(name = "full_size")
        val bookFullSize: String,
        @ColumnInfo(name = "update_size")
        val bookUpdateSize: String,
        @ColumnInfo(name = "genres")
        val bookGenres: String,
        @ColumnInfo(name = "description")
        val bookDescription: String,
        val site: String,
        val favourite: Boolean

)