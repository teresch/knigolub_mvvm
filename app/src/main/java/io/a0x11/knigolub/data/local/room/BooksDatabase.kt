package io.a0x11.knigolub.data.local.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import io.a0x11.knigolub.data.model.local.Favourite
import io.a0x11.knigolub.data.model.local.Cache

@Database(entities = [Favourite::class, Cache::class], version = 1)
abstract class BooksDatabase : RoomDatabase() {

    abstract val favouriteDao: FavouriteDao
    abstract val cacheDao: CacheDao

}