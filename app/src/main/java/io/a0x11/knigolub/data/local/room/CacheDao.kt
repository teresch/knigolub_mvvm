package io.a0x11.knigolub.data.local.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.a0x11.knigolub.data.model.local.Cache
import io.reactivex.Single

@Dao
interface CacheDao {

    @Insert()
    fun cacheBook(cache: Cache)

    @Query("DELETE FROM cache")
    fun clearCache()

    @Query("SELECT * FROM cache")
    fun getCache(): Single<List<Cache>>

}