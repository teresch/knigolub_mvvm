package io.a0x11.knigolub.data.model.remote

import com.google.gson.annotations.SerializedName

data class Samlib(@SerializedName("response") val samlibBooks: List<SamlibBook>) {

    data class SamlibBook(
                val event: String,
                val time: Long,
                val url: String,
                val type: Int,
                val rating: String,
                val author: String,
                val site: Int,
                val utype: Int,
                val title: String,
                val genre: String,
                val description: String,
                val size: Int?,
                val oldsize: Int?
        )
}
