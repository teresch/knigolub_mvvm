package io.a0x11.knigolub.data.remote

import io.a0x11.knigolub.data.model.remote.Samlib
import io.reactivex.Single
import retrofit2.http.GET

interface Api {

@GET("api/get/samlib/?app_id=356a192b7913b04c54574d18c28d46e6395428ab")
fun samlibList(): Single<Samlib>
}