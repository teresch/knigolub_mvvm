package io.a0x11.knigolub.injection.module

import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.a0x11.knigolub.injection.annotation.PerActivity
import io.a0x11.knigolub.presentation.base.ViewModelFactory
import io.a0x11.knigolub.presentation.screen.home.HomeActivity
import io.a0x11.knigolub.presentation.screen.home.HomeModule

@Module
abstract class UiModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @PerActivity
    @ContributesAndroidInjector(modules = [HomeModule::class])
    abstract fun contributeHomeActivity(): HomeActivity

}