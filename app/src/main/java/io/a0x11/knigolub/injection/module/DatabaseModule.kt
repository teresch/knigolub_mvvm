package io.a0x11.knigolub.injection.module

import android.arch.persistence.room.Room
import dagger.Module
import dagger.Provides
import io.a0x11.knigolub.App
import io.a0x11.knigolub.data.local.room.FavouriteDao
import io.a0x11.knigolub.data.local.room.BooksDatabase
import io.a0x11.knigolub.data.local.room.CacheDao
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(app: App): BooksDatabase {
        return Room.databaseBuilder(app, BooksDatabase::class.java, "books.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    @Provides
    @Singleton
    fun provideBookDao(db: BooksDatabase): FavouriteDao = db.favouriteDao

    @Provides
    @Singleton
    fun provideDetailDao(db: BooksDatabase): CacheDao = db.cacheDao

}