package io.a0x11.knigolub.injection.component

import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import io.a0x11.knigolub.App
import io.a0x11.knigolub.injection.module.AppModule
import io.a0x11.knigolub.injection.module.DatabaseModule
import io.a0x11.knigolub.injection.module.RouterModule
import io.a0x11.knigolub.injection.module.UiModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AppModule::class,
            UiModule::class,
            RouterModule::class,
            DatabaseModule::class,
            AndroidInjectionModule::class,
            AndroidSupportInjectionModule::class
        ]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()

}