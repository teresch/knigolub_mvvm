package io.a0x11.knigolub.utils.diff

import android.databinding.ObservableArrayList
import android.support.v7.util.DiffUtil
import io.a0x11.knigolub.presentation.model.ObservableItemBook

class BookDiffUtilCallback (private val oldList: ObservableArrayList<ObservableItemBook>,
                            private val newList: ObservableArrayList<ObservableItemBook>): DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size
    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].bookUrl == newList[newItemPosition].bookUrl &&
                    oldList[oldItemPosition].bookFullSize == newList[newItemPosition].bookFullSize &&
                    oldList[oldItemPosition].bookUpdateDate == newList[newItemPosition].bookUpdateDate


    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                oldList[oldItemPosition].bookUpdateSize == newList[newItemPosition].bookUpdateSize

}