package io.a0x11.knigolub.utils

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.annotation.LayoutRes
import android.support.v4.app.FragmentActivity
import io.a0x11.knigolub.presentation.base.BaseViewModel

fun <T : ViewDataBinding> FragmentActivity.getDataBinding(@LayoutRes layoutId: Int
): T = DataBindingUtil.setContentView(this, layoutId)

inline fun <reified T : BaseViewModel> FragmentActivity.getViewModel(
        factory: ViewModelProvider.Factory = ViewModelProvider.AndroidViewModelFactory(application)
): T = ViewModelProviders.of(this, factory).get(T::class.java)

fun <T: android.databinding.Observable> T.addOnPropertyChanged(callback: (T) -> Unit) =
        object: android.databinding.Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(observable: android.databinding.Observable, i: Int) =
                    callback(observable as T)
        }.also { addOnPropertyChangedCallback(it) }