package io.a0x11.knigolub.utils

import android.content.Context
import android.databinding.BindingAdapter
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import com.jakewharton.rxrelay2.PublishRelay
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.miguelcatalan.materialsearchview.R.id.searchTextView
import eu.davidea.flipview.FlipView
import io.a0x11.knigolub.R
import io.a0x11.knigolub.databinding.ItemBookBinding
import io.a0x11.knigolub.presentation.model.ObservableItemBook
import io.a0x11.knigolub.utils.recycler.BaseAdapter
import io.a0x11.knigolub.utils.recycler.DataBoundViewHolder
import io.a0x11.knigolub.utils.recycler.DividerItemDecoration
import java.text.SimpleDateFormat
import java.util.*
import io.a0x11.knigolub.utils.diff.BookListUpdateCallback


//RecyclerView
@BindingAdapter("divideItemDecoration")
fun RecyclerView.applyItemDecoration(ignore: Boolean) =
        addItemDecoration(DividerItemDecoration(context))

@BindingAdapter("books", "onBookClick", "onFlipClick", "onFavClick")
fun RecyclerView.books(books: ObservableArrayList<ObservableItemBook>, onBookClick: PublishRelay<String>, flipClickPosition: PublishRelay<Int>,
                       toggleClickPosition: PublishRelay<Int>) {

    if (adapter == null){
        adapter = BaseAdapter(R.layout.item_book,books,
                object : BaseAdapter.HolderBinding {
                    override fun customize(vh: DataBoundViewHolder<*>) {
                        val binding = vh.binding as ItemBookBinding
                        binding.book = books[vh.adapterPosition]
                        binding.root.setOnClickListener { onBookClick.accept(books[vh.adapterPosition].bookUrl) }
                        binding.flipView.setOnClickListener {
                            books[vh.adapterPosition].flipped.set(true)
                            flipClickPosition.accept(vh.adapterPosition)
                        }
                        binding.toggleButton.setOnClickListener { toggleClickPosition.accept(vh.adapterPosition) }
                    }
                }
        )
    }else{
        val bookListUpdateCallback = BookListUpdateCallback()
        bookListUpdateCallback.bind(adapter!! as BaseAdapter)
        (adapter!! as BaseAdapter).setItems(books, bookListUpdateCallback)
        scrollToPosition(bookListUpdateCallback.position)
    }
}

@BindingAdapter("fabScroll", "onFabClick")
fun RecyclerView.fabBehavior(catchFabVisibility: PublishRelay<Boolean>, scrollToTop: ObservableBoolean){
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (dy > 0) {
                catchFabVisibility.accept(true)
            } else if (dy < 0) {
                catchFabVisibility.accept(false)
            }
        }
    })
    if (scrollToTop.get()) {
        scrollToPosition(0)
        scrollToTop.set(false)
    }
}

//FlipView
@BindingAdapter("setFrontText")
fun FlipView.frontText(bookTitle: String) {

    if (!bookTitle.isEmpty()) setFrontText(bookTitle.substring(0, 1))
    else setFrontText("")
}

@BindingAdapter("setFlipSide")
fun FlipView.flipSide(flipSide: ObservableBoolean) {

    flip(flipSide.get())
}

//TextView
@BindingAdapter("setDate")
fun TextView.formDate(date: Long) {

    text = if (DateUtils.isToday(date)) SimpleDateFormat("HH:mm", Locale.getDefault()).format(date)
    else SimpleDateFormat("dd MMM", Locale.getDefault()).format(date)
}

//MaterialSearchView
@BindingAdapter("onQueryChange")
fun MaterialSearchView.queryChange(searchQuery: PublishRelay<String>) {

    setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean = false

        override fun onQueryTextChange(newText: String): Boolean {
            searchQuery.accept(newText)
            return false
        }
    })
}

@BindingAdapter("overrideKeyboardLogic")
fun MaterialSearchView.keyboardLogic(ignore: Boolean) {

    val editText = findViewById<EditText>(searchTextView)
    editText.imeOptions = EditorInfo.IME_FLAG_NO_EXTRACT_UI or EditorInfo.IME_ACTION_DONE
    editText.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editText.windowToken,
                    InputMethodManager.RESULT_UNCHANGED_SHOWN)
            return@OnEditorActionListener true
        }
        false
    })
}

//FAB
@BindingAdapter("setVisibility")
fun FloatingActionButton.scrollBehavior(boolean: Boolean) {
    if (boolean)
        show()
    else
        hide()
}
