package io.a0x11.knigolub.utils.recycler

import android.databinding.ObservableArrayList
import android.databinding.ViewDataBinding
import android.support.annotation.LayoutRes
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import io.a0x11.knigolub.presentation.model.ObservableItemBook
import io.a0x11.knigolub.utils.diff.BookDiffUtilCallback
import io.a0x11.knigolub.utils.diff.BookListUpdateCallback


open class BaseAdapter(@LayoutRes private val layout: Int, books: ObservableArrayList<ObservableItemBook>,
                       private val holderBindingListener: HolderBinding) : RecyclerView.Adapter<DataBoundViewHolder<*>>() {

    private val books = ObservableArrayList<ObservableItemBook>()

    init {
        this.books.addAll(books)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBoundViewHolder<*> =
            DataBoundViewHolder.create<ViewDataBinding>(parent, layout)

    override fun onBindViewHolder(holder: DataBoundViewHolder<*>, position: Int) = holderBindingListener.customize(holder)

    override fun getItemCount() = books.size

    interface HolderBinding {
        fun customize(vh: DataBoundViewHolder<*>)
    }

    fun setItems(books: ObservableArrayList<ObservableItemBook>, bookListUpdateCallback: BookListUpdateCallback) {
        val diffResult = DiffUtil.calculateDiff(BookDiffUtilCallback(this.books, books))
        diffResult.dispatchUpdatesTo(bookListUpdateCallback)

        with(this.books) {
            clear()
            addAll(books)
        }
    }
}
