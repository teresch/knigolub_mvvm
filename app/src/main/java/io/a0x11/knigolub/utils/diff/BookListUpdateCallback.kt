package io.a0x11.knigolub.utils.diff

import android.support.v7.util.ListUpdateCallback
import io.a0x11.knigolub.utils.recycler.BaseAdapter

class BookListUpdateCallback : ListUpdateCallback {
    var position = -1
    var adapter: BaseAdapter? = null

    fun bind(adapter: BaseAdapter) {
        this.adapter = adapter
    }

    override fun onChanged(position: Int, count: Int, payload: Any?) {
        adapter!!.notifyItemRangeChanged(position, count, payload)
    }

    override fun onInserted(position: Int, count: Int) {
        if (this.position == -1 || this.position > position) {
            this.position = position
        }
        adapter!!.notifyItemRangeInserted(position, count)
        this.position = 0
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        adapter!!.notifyItemMoved(fromPosition, toPosition)
    }

    override fun onRemoved(position: Int, count: Int) {
        adapter!!.notifyItemRangeRemoved(position, count)
        this.position = 0
    }
}