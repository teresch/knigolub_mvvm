package io.a0x11.knigolub.presentation.base

import android.arch.lifecycle.ViewModel
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

abstract class BaseViewModel : ViewModel(),BaseViewModelFacade{

    protected val disposables = CompositeDisposable()

    private  val messageToast = PublishRelay.create<String>()

    override fun observeMessage(): Observable<String> = messageToast

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    protected fun handleError(error: Throwable) {
        Timber.e(error)
        error.message?.let { messageToast.accept(it) }
    }
}