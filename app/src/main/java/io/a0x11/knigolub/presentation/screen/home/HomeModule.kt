package io.a0x11.knigolub.presentation.screen.home

import android.arch.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.a0x11.knigolub.injection.annotation.PerActivity
import io.a0x11.knigolub.injection.annotation.ViewModelKey

@Module
abstract class HomeModule {

    @Binds
    @IntoMap
    @PerActivity
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindViewModel(viewModel: HomeViewModel): ViewModel

}