package io.a0x11.knigolub.presentation.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import dagger.android.AndroidInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import org.jetbrains.anko.toast
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportAppNavigator
import timber.log.Timber
import javax.inject.Inject
import android.R.menu
import io.a0x11.knigolub.R


abstract class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    protected open val navigator = BaseNavigator()

    protected val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    protected open fun setViewModel(viewModelFacade: BaseViewModelFacade) = initViewModelDisposables(viewModelFacade)

    private fun initViewModelDisposables(viewModelFacade: BaseViewModelFacade) =
            viewModelFacade.observeMessage()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { showToast(it) }
                    .addTo(disposables)

    protected fun handleError(error: Throwable) {
        error.message?.let {
            Timber.e(error)
            showToast(it)
        }
    }

    private fun showToast(message: String) = toast(message)

    protected open inner class BaseNavigator : SupportAppNavigator(this, 0) {

        override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? = null

        override fun createFragment(screenKey: String?, data: Any?): Fragment?  = null
    }
}