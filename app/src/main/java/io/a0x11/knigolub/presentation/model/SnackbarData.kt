package io.a0x11.knigolub.presentation.model

data class SnackbarData(

        val authorPage: String,
        val bookGenres: String,
        val position: Int

)