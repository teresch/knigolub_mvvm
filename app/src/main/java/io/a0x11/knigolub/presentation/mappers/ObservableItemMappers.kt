package io.a0x11.knigolub.presentation.mappers

import android.databinding.ObservableBoolean
import io.a0x11.knigolub.data.model.local.Cache
import io.a0x11.knigolub.domain.model.Book
import io.a0x11.knigolub.presentation.model.ObservableItemBook

fun mapBookToObservableItemBook(book: Book): ObservableItemBook =
        ObservableItemBook(
                book.bookTitle,
                book.bookUrl,
                book.author,
                book.authorUrl,
                book.bookUpdateDate,
                book.bookFullSize,
                book.bookUpdateSize,
                book.bookGenres,
                book.bookDescription,
                ObservableBoolean(book.favourite),
                ObservableBoolean(false)
        )

fun mapCacheToObservableItemBook(book: Cache): ObservableItemBook =
        ObservableItemBook(
                book.bookTitle,
                book.bookUrl,
                book.author,
                book.authorUrl,
                book.bookUpdateDate,
                book.bookFullSize,
                book.bookUpdateSize,
                book.bookGenres,
                book.bookDescription,
                ObservableBoolean(book.favourite),
                ObservableBoolean(false)
        )