package io.a0x11.knigolub.presentation.screen.home

import io.a0x11.knigolub.presentation.model.SnackbarData
import io.reactivex.Observable

interface HomeFacade {

    fun showSnackbar(): Observable<SnackbarData>

    fun snackbarDismissed(position: Int)

}