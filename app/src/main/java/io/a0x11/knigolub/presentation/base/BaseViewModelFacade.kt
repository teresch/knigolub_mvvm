package io.a0x11.knigolub.presentation.base

import io.reactivex.Observable

interface BaseViewModelFacade {
    fun observeMessage(): Observable<String>
}