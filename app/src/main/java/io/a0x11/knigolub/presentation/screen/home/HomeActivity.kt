package io.a0x11.knigolub.presentation.screen.home

import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.SearchView
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.miguelcatalan.materialsearchview.MaterialSearchView
import io.a0x11.knigolub.R
import io.a0x11.knigolub.Screens
import io.a0x11.knigolub.databinding.ActivityHomeBinding
import io.a0x11.knigolub.presentation.base.BaseActivity
import io.a0x11.knigolub.presentation.model.SnackbarData
import io.a0x11.knigolub.utils.getDataBinding
import io.a0x11.knigolub.utils.getViewModel
import io.reactivex.rxkotlin.addTo
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.toast
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class HomeActivity : BaseActivity() {

    override val navigator: BaseNavigator = NavigatorImpl()

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    @Inject
    lateinit var router: Router

    private lateinit var facade: HomeFacade
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getDataBinding(R.layout.activity_home)
        binding.viewModel = getViewModel(factory)
        setViewModel(binding.viewModel!!)

        setSupportActionBar(binding.toolbar)

        facade = binding.viewModel as HomeFacade

        facade.showSnackbar()
                .subscribe({ makeSnackbar(it) }, {})
                .addTo(disposables)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu_main, menu)

        binding.searchView.setMenuItem(menu!!.findItem(R.id.action_search))

        return true
    }

    private fun makeSnackbar(snackbarData: SnackbarData) {
                snackbar(binding.coordinator, "Жанры: ${snackbarData.bookGenres}")
                .setActionTextColor(ContextCompat.getColor(this, R.color.orange))
                .setAction(getString(R.string.auth_page)) {
                    router.navigateTo(Screens.INTERNET, snackbarData.authorPage)
                }.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                        facade.snackbarDismissed(snackbarData.position)
                        super.onDismissed(snackbar, event)
                    }
                })
    }

    private inner class NavigatorImpl : BaseNavigator() {

        override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? =
                when (screenKey) {
                    Screens.INTERNET -> Intent(Intent.ACTION_VIEW, Uri.parse("http://samlib.ru/${data.toString()}")) //TODO Сделать из SharedPrefs
                    else -> null
                }

    }
}
