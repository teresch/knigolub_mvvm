package io.a0x11.knigolub.presentation.screen.home

import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import com.jakewharton.rxrelay2.PublishRelay
import io.a0x11.knigolub.Screens
import io.a0x11.knigolub.domain.interactors.SamlibInteractor
import io.a0x11.knigolub.presentation.base.BaseViewModel
import io.a0x11.knigolub.presentation.model.ObservableItemBook
import io.a0x11.knigolub.presentation.model.SnackbarData
import io.a0x11.knigolub.utils.addOnPropertyChanged
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


class HomeViewModel @Inject constructor(private val samlibInteractor: SamlibInteractor, private val router: Router) : BaseViewModel(), HomeFacade {

    val samlibBooks = ObservableArrayList<ObservableItemBook>()
    private val searchList = ArrayList<ObservableItemBook>()

    val bookClick = PublishRelay.create<String>()
    val flipClick = PublishRelay.create<Int>()
    val toggleClick = PublishRelay.create<Int>()
    val snackbarData = PublishRelay.create<SnackbarData>()
    val searchQuery = PublishRelay.create<String>()
    val catchFabVisibility = PublishRelay.create<Boolean>()

    val refreshing: ObservableBoolean = ObservableBoolean(false)
    val fabVisibility: ObservableBoolean = ObservableBoolean(false)
    val scrollToTop: ObservableBoolean = ObservableBoolean(false)

    init {

        Single.concat(provideCache(), provideApiSingle())
                .subscribe(this::listToObservableItemBook, this::handleError)
                .addTo(disposables)

        bookClick
                .subscribe({ router.navigateTo(Screens.INTERNET, it) }, this::handleError)
                .addTo(disposables)

        flipClick
                .subscribe({ snackbarData.accept(fillSnackbarData(it)) }, this::handleError)
                .addTo(disposables)

        toggleClick
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(this::favButtonLogic, this::handleError)
                .addTo(disposables)

        searchQuery
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(this::searchQuery, this::handleError)
                .addTo(disposables)


        catchFabVisibility
                .subscribe(this::setFabVisibility, this::handleError)
                .addTo(disposables)

    }

    override fun showSnackbar(): Observable<SnackbarData> = snackbarData

    override fun snackbarDismissed(position: Int) {
        if (samlibBooks.size > position)
            samlibBooks[position].flipped.set(false)
    }

    fun onRefresh() = provideApiSingle()
            .subscribe(this::listToObservableItemBook, this::handleError)
            .addTo(disposables)

    fun scrollToTop() {
        scrollToTop.set(true)
        scrollToTop.addOnPropertyChanged { fabVisibility.set(false) }
    }

    private fun provideCache() =
            samlibInteractor.samlibCacheList()
                    .observeOn(AndroidSchedulers.mainThread())

    private fun provideApiSingle() =
            samlibInteractor.samlibApiList()
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnEvent { _, _ -> refreshing.set(false) }
                    .doOnSubscribe { refreshing.set(true) }

    private fun fillSnackbarData(position: Int): SnackbarData =
            SnackbarData(
                    samlibBooks[position].authorUrl,
                    samlibBooks[position].bookGenres,
                    position
            )

    private fun favButtonLogic(position: Int) {
        if (samlibBooks[position].favouriteBook.get()) {
            samlibInteractor.deleteBookFromFavourite(samlibBooks[position].bookUrl)
            notifyObservableBoolean(position, false)
        } else {
            samlibInteractor.makeBookFavourite(samlibBooks[position])
            notifyObservableBoolean(position, true)
        }
    }

    private fun setFabVisibility(boolean: Boolean) = fabVisibility.set(boolean)

    private fun searchQuery(query: String) {
        val charText = query.toLowerCase(Locale.getDefault())
        samlibBooks.clear()
        if (charText.isEmpty()) {
            for (book in searchList) {
                book.flipped.set(false)
                samlibBooks.add(book)
            }
        } else {
            for (book in searchList) {
                if (book.author.toLowerCase(Locale.getDefault()).contains(charText) ||
                        book.bookTitle.toLowerCase(Locale.getDefault()).contains(charText) ||
                        book.bookGenres.toLowerCase(Locale.getDefault()).contains(charText)) {
                    book.flipped.set(false)
                    samlibBooks.add(book)
                }
            }
        }
    }

    private fun listToObservableItemBook(list: List<ObservableItemBook>) {
        with(samlibBooks) {
            clear()
            addAll(list)
        }
        with(searchList) {
            clear()
            addAll(list)
        }
    }

    private fun notifyObservableBoolean(position: Int, set: Boolean) {
        for (sb in samlibBooks) {
            if (sb.bookUrl == samlibBooks[position].bookUrl)
                sb.favouriteBook.set(set)
        }
    }
}