package io.a0x11.knigolub.domain.mappers

import io.a0x11.knigolub.data.model.local.Favourite
import io.a0x11.knigolub.data.model.remote.Samlib
import io.a0x11.knigolub.domain.model.Book

fun mapSamlibResponse(responseList: List<Samlib.SamlibBook>, listFromDB: List<Favourite>): List<Book> {
    val favDBList: ArrayList<String> = ArrayList()
    val bookList: ArrayList<Book> = ArrayList()

    for (bookEntity in listFromDB)
        favDBList.add(bookEntity.bookUrl)
    for (samlibResponse in responseList) {

        if (samlibResponse.site != 3) {
            if (samlibResponse.type == 2) {
                bookList.add(parseType2(samlibResponse, favDBList))
            } else {
                bookList.add(parseType1(samlibResponse, favDBList))
            }
        }

    }
    return bookList
}

private fun parseType1(samlibResponse: Samlib.SamlibBook, favList: ArrayList<String>): Book {
    return Book(
            samlibResponse.title,
            samlibResponse.url,
            samlibResponse.author,
            samlibResponse.url.substring(0, samlibResponse.url.lastIndexOf("/")),
            samlibResponse.time * 1000,
            "${samlibResponse.size.toString()} Кб",
            formUpdateSize(1, samlibResponse),
            samlibResponse.genre,
            samlibResponse.description.replace("' (", "").replace("):", ""),
            "samlib",
            favList.contains(samlibResponse.url)
    )
}

private fun parseType2(samlibResponse: Samlib.SamlibBook, favList: ArrayList<String>): Book {
    return with(samlibResponse.description.replace("' (", "").replace("):", "")) {
        Book(
                this.substring(this.indexOf("дение ") + 6, this.indexOf(" (")),
                formBookUrl(this),
                samlibResponse.author,
                formAuthorUrl(this),
                samlibResponse.time * 1000,
                "${this.substring(this.lastIndexOf(" (") + 2, this.indexOf("КБ )"))} Кб",
                formUpdateSize(2, samlibResponse),
                this.substring(this.lastIndexOf(" ) ") + 3, this.indexOf(" [id=")),
                this,
                "samlib",
                favList.contains(formBookUrl(this))
        )
    }//
}

private fun formUpdateSize(type: Int, samlibResponse: Samlib.SamlibBook): String {
    if (type == 1)
        with(samlibResponse.size!! - samlibResponse.oldsize!!) {
            return if (this > 0) "+${this}" else "${this}"
        }
    else
        return "New"
}

private fun formBookUrl(desc: String) = desc.substring(desc.indexOf("[id="), desc.lastIndexOf(" ]")).split(" ")[1]
        .replace("http://samlib.ru/","")

private fun formAuthorUrl(desc: String) = desc.substring(desc.indexOf("samlib.ru") + 10, desc.indexOf(" \n"))