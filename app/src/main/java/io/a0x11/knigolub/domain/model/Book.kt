package io.a0x11.knigolub.domain.model

data class Book (

        val bookTitle: String,
        val bookUrl: String,
        val author: String,
        val authorUrl: String,
        val bookUpdateDate: Long,
        val bookFullSize: String,
        val bookUpdateSize: String,
        val bookGenres: String,
        val bookDescription: String,
        val site: String,
        var favourite: Boolean

)