package io.a0x11.knigolub.domain.interactors

import io.a0x11.knigolub.data.model.local.Favourite
import io.a0x11.knigolub.data.model.remote.Samlib
import io.a0x11.knigolub.data.repository.SamlibRepository
import io.a0x11.knigolub.presentation.mappers.mapBookToObservableItemBook
import io.a0x11.knigolub.domain.mappers.mapSamlibResponse
import io.a0x11.knigolub.domain.model.Book
import io.a0x11.knigolub.presentation.mappers.mapCacheToObservableItemBook
import io.a0x11.knigolub.presentation.model.ObservableItemBook
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SamlibInteractor @Inject constructor(private val samlibRepository: SamlibRepository) {

    fun samlibApiList(): Single<List<ObservableItemBook>> =
            Observable.combineLatest<Samlib, List<Favourite>, List<Book>>(
                    samlibRepository.samlibApiList().toObservable(),
                    samlibRepository.getAllFavouriteBooks().toObservable(),
                    BiFunction { t1, t2 -> mapSamlibResponse(t1.samlibBooks, t2) })
                    .map { clearCache(it) }
                    .flatMap { Observable.fromIterable(it) }
                    .filter { it.bookUpdateSize != "0" }
                    .doOnNext { samlibRepository.cacheBook(it) }
                    .map { mapBookToObservableItemBook(it) }
                    .toList()
                    .subscribeOn(Schedulers.io())

    fun samlibCacheList(): Single<List<ObservableItemBook>> =
            samlibRepository.getCache()
                    .flatMapObservable { Observable.fromIterable(it) }
                    .map { mapCacheToObservableItemBook(it) }
                    .toList()
                    .subscribeOn(Schedulers.io())

    fun makeBookFavourite(book: ObservableItemBook) = samlibRepository.makeBookFavourite(book)

    fun deleteBookFromFavourite(bookUrl: String) = samlibRepository.deleteBookFromFavourite(bookUrl)

    fun clearCache(it: List<Book>): List<Book> {
        if (it.isNotEmpty())
            samlibRepository.clearCache()
        return it
    }

}
